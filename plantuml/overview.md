# PlantUML integration for GitLab.com

PlantUML is a web application that generates UML diagrams on the fly.

https://github.com/plantuml/plantuml-server

Since release 8.6, GitLab has supported integration with a PlantUML
server https://docs.gitlab.com/ee/administration/integration/plantuml.html .

This readiness review is for enabling PlantUML integration on GitLab.com
which is a popular request from the community https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/2163
by running PlantUML in a production Kubernetes cluster.

**Operational runbook - https://gitlab.com/gitlab-com/runbooks/blob/master/howto/k8s-plantuml-operations.md**

## Table of contents

  * [Architecture overview](#architecture-and-overview)
  * [Configuration](#configuration)
  * [Risk assessment](#risk-assessment-and-blast-radius-of-failures)
  * [Security considerations](#security-considerations)
  * [Application upgrade/rollback](#application-upgrade-and-rollback)
  * [Observability and Monitoring/Logging](#observability-and-monitoring)
  * [Testing](#testing)

## Architecture and Overview

PlantUML is a stateless service that generates UML and state diagrams.

### Diagram Examples

* Very simple diagram https://pre.gitlab.com/jarv/plantuml-testing/issues/6
* Diagram with imported fonts https://pre.gitlab.com/jarv/plantuml-testing/issues/5
* Embedding an image https://pre.gitlab.com/jarv/plantuml-testing/issues/2
* Handwritten example https://pre.gitlab.com/jarv/plantuml-testing/issues/9

### High level overview

* A PlantUML block is included in an issue or markdown document:

    ```plantuml
      @startuml

      !include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml
      !define ICONURL  https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.4.0
      !define FONTAWESOME https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/font-awesome-5


      ' global definition
      !define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.4.0
      !includeurl ICONURL/common.puml
      !includeurl ICONURL/devicons2/gitlab.puml
      !includeurl ICONURL/font-awesome/gitlab.puml
      !includeurl ICONURL/font-awesome-5/gitlab.puml

      !includeurl ICONURL/devicons/jenkins.puml

      !includeurl ICONURL/devicons2/kubernetes.puml


      !include FONTAWESOME/users.puml
      !include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

      AddElementTag("v1.0", $fontColor="#d73027", $borderColor="#d73027")
      AddElementTag("v1.1", $fontColor="#ffffbf", $borderColor="#ffffbf")
      AddElementTag("k8s", $fontColor="#1d1d21", $borderColor="#9782ff")
      AddElementTag("dev", $fontColor="red", $borderColor="#9782ff")
      AddElementTag("test", $fontColor="green", $borderColor="#9782ff")
      AddElementTag("prod", $fontColor="blue", $borderColor="#9782ff")


      AddElementTag("gitlab", $fontColor="#ff6456", $borderColor="#9782ff")
      AddRelTag("dev", $textColor="red", $lineColor="red")
      AddRelTag("test", $textColor="green", $lineColor="green")
      AddRelTag("prod", $textColor="blue", $lineColor="blue")


      System_Boundary(Roles, "Roles"){
        Person(dev, "Développeur", "", $tags="dev",$sprite="users")
        Person(testeur, "testeur", "Personne qui qualifie le produit","", $tags="test",$sprite="users")
        Person(ops, "Agent de production", "Agent qui déploie le produit","", $tags="prod",$sprite="users")
      }
      DEV_JENKINS(ic,"Jenkins",node)

      System_Boundary(banking_system, "Build"){
        DEV2_GITLAB(scm,"Gitlab",rectangle,#ff6456)
        Container(gestArt, "Artifactory", "", "Gestionnaire des artefacts", $tags="dev")
      }
      Container(drops, "Drops", "", "Gestionnaire de diffusion")

      DEV2_KUBERNETES(intDefaut, "k8s-dev",rectangle,#4161ff)
      DEV2_KUBERNETES(pds, "k8s-simu",rectangle,#4161ff)
      DEV2_KUBERNETES(prod, "k8s-prod",rectangle,#4161ff)



      Rel_D(dev, scm, "Git push",$tags=dev)

      Rel_D(testeur, ic, "Lancement Job Drops",$tags=test)
      Rel_D(ops, drops, "Déploiement du produit depuis Drops",$tags=prod)
      Rel_D(ic, gestArt, "Docker pull",$tags=dev)
      Rel_D(ic, drops, "Appel API drops depuis Jenkins", "https",$tags=dev)

      Rel_D(ic, drops, "Appel API drops depuis Jenkins", "https",$tags=test)

      Rel_D(scm, gestArt, "Docker push",$tags=dev)
      Rel_R(scm, ic, "Appel auto du Job Jenkins déploiement Drops ",$tags=dev)

      Rel_D(drops, intDefaut, "Déploiement du produit",$tags=dev)
      Rel_D(drops, pds, "Déploiement du produit",$tags=test)
      Rel_D(drops, prod, "Déploiement du produit",$tags=prod)


      SHOW_FLOATING_LEGEND()

      @enduml

    ```

* GitLab replaces this block with an inline image link where the diagram
is embeded in the URL, for example:

```
https://plantuml.example.com/png/U9npoazIqBLJ24uiIinGi5B8o5Ja0W0s7mKa

```
* When the PlantUML service receives this GET request, it returns a `png` image of the
diagram.


```mermaid
sequenceDiagram
    participant Browser
    participant GitLab.com
    participant CDN
    participant Nginx
    participant PlantUML
    Browser->>GitLab.com: Create a PlantUML markdown block
    GitLab.com->>Browser: HTML with an inline image /png/XXXXX
    Note right of GitLab.com: GitLab replaces the <br />block with an<br />image link<br />/png/XXXXX
    Browser->>CDN: Requests /png/XXXXX
    Note right of CDN: Previously requested<br />images will be cached
    CDN-->>Browser: Returns cached /png/XXXX as png image
    CDN->>Nginx: Requests /png/XXXXX
    Note right of Nginx: Nginx Returns 301<br />redirect unless<br />it is a GET<br />request for /png
    Note right of Nginx: Evaluates rate<br />limiting rules<br />before proxing<br />the request
    Nginx->>PlantUML: Requests /png/XXXXX
    loop PlantUML
        PlantUML-->PlantUML: Generates Image
    end
    PlantUML->>Nginx: Returns png image
    Note left of Nginx: Modifies headers<br />for security
    Nginx->>Browser: Returns /png/XXXX as png image
```

The PlantUML service is deployed into the existing production GKE cluster in a
separate namespace named `plantuml`.

```mermaid
graph TB;
  a(browser) --> b(L7 GCP LB:443);
  subgraph GKE
    b --> c(nginx:80);
    c --> d(PlantUML:8080);
  end
  linkStyle 0 stroke-width:2px,fill:none,stroke:blue;
  linkStyle 1 stroke-width:2px,fill:none,stroke:red;
  linkStyle 2 stroke-width:2px,fill:none,stroke:red;
```

All requests (blue) to the LB ingress are over https, internal connections (red) in the
cluster are not encrypted.

---

## Configuration

PlantUML uses a
[GitLab maintained helm chart](https://gitlab.com/gitlab-org/charts/plantuml).

We have modified the configuration from a standard PlantUML installation
in the following ways:

* Only the `/png` endpoint is supported, other endpoints like `/uml` and
  `/proxy` are disabled and will redirect the user back to GitLab. These
  endpoints are disabled because they are not necessary for GitLab integration.
* Aggressive rate limiting is configured at Nginx for creating new diagrams
* Two headers are dropped from PlantUML's https response:
  * `access-control-allow-origin`: Not needed in GitLab's configuration
  * `x-powered-by`: This exposes the server version and is dropped for security
    reasons

## Risk Assessment and Blast radius of failures

The PlantUML service is deployed in one or more pods on nodes
that span at least three availability zones in a single region. The PlantUML service is also deployed into a namespace separate from the main GitLab application. 

If the PlantUML service becomes unavailable the impact on GitLab.com will be
minimal:

* All image links to PlantUML diagrams will be broken, but issues will otherwise
  render properly
* Users will be unable to create new diagrams or preview them in issues

Nginx rate limiting for creating new charts is configured to prevent a single
bad actor from disabling the service or causing the cluster to scale. The
current rate limit per IP will be set between `2 req/s` and `5 req/s` per pod.

_Note: This rate limit only applies for creating diagrams from the service, not fetching cached versions from the CDN_

## Resource Management

PlantUML has per environment requests and resource limits, configured in the
[`k8s-workloads/plantuml` project](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/plantuml/blob/7850985e67984d363b31ed888674325fab84e03b/pre.yaml#L14-20)

The service is memory bound and defaults were chosen based on load testing where
we are seeing the service consume about ~300MB of memory and up to a .5 cpu
cores under what we think will be typical load.


## Security Considerations

### Network Access

- The plantuml service is hosted on a different domain, `gitlab-static.net`,
  this prevents cookies, specifically the gitlab session cookie from being
  forwarded to the service
- All egress traffic to the internal network and the metadata service is blocked with a [Kubernetes networkpolicy](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/plantuml/blob/cc667a6ff4a0c5563841de3c76f5a0c098c8bbaa/values.yaml#L9-28)
- [Nginx configuration](https://gitlab.com/gitlab-org/charts/plantuml/blob/8d080485f58020a08b75a889f1fb81159fa93195/templates/configmap.yaml#L54-56) denies all request types other than GET to the `png` endpoint
- [Nginx is configured to issue a redirect for everything except the `/png` endpoint](https://gitlab.com/gitlab-org/charts/plantuml/blob/87905d4c5d098f659324f218759dfa34ee015bfc/templates/configmap.yaml#L15-17),
  this blocks unused features of the PlantUML service like `/uml` and `/proxy`

### Abuse

Careful consideration was given to whether this service can be abused by a bad
actor. Because creating diagrams is a memory intensive operation and can cause
the service to fall over or scale unnecessarily, rate limits were added to
the [Nginx config](https://gitlab.com/gitlab-org/charts/plantuml/blob/8d080485f58020a08b75a889f1fb81159fa93195/templates/configmap.yaml#L10)
 that will limit diagram creation.

The rate limit for creating diagrams will be between `2 req/sec` and `5 req/sec`
and will be evaluated and monitored when we go live.

Rate limiting will be monitored by viewing Nginx logs in central logging for
`429` status codes by Nginx or by viewing `429` response codes on
[dashboards.gitlab.net](https://dashboards.gitlab.net/d/plantuml-main/plantuml-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=pre&var-cluster=pre-gitlab-gke&from=1568125360258&to=1568135416227)
for cache misses on the service

### Headers

The following headers are set in the response when the browser makes a request
to the service:

```
$ curl -I https://pre.plantuml.gitlab-static.net/png
HTTP/2 200
server: nginx
date: Mon, 09 Sep 2019 12:50:07 GMT
content-type: image/png
content-length: 13040
expires: Sat, 14 Sep 2019 12:50:07 GMT
last-modified: Fri, 24 May 2019 17:10:00 GMT
cache-control: public, max-age=432000
etag: "6k04YwmUDcKOApAaotmlunTLW00"
x-plantuml-diagram-description: (Empty)
x-patreon: Support us on http://plantuml.com/patreon
x-donate: http://plantuml.com/paypal
x-frame-options: SAMEORIGIN
via: 1.1 google
alt-svc: clear
```

### Confidential Issues

Every plantuml link is public, meaning that if you include a diagram in a confidential issue and you know the URL then it is possible to view the diagram in the issue.
The data of the diagram is encoded in the URL itself, so given this the data in the URL is private, which means if you have the URL you have the data anyway.


## Application Upgrade and Rollback

```mermaid
sequenceDiagram
    participant Administrator
    participant GitLab.com
    Administrator->>GitLab.com: Submits an MR for review
    loop CI
        GitLab.com->>GitLab.com: Syntax checks
    end
    GitLab.com->>ops.gitlab.net: branch mirrored to ops.gitlab.net
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Dryrun apply on all envs
    end
    Administrator->>GitLab.com: MR merged to master
    loop CI
        GitLab.com->>GitLab.com: Syntax checks
    end
    GitLab.com->>ops.gitlab.net: master mirrored to ops.gitlab.net
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to non-production envs
    end
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to production
    end
```

These versions are set for the PlantUML service:

* `CHART_VERSION` which is set in the [configuration project](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/plantuml/blob/7850985e67984d363b31ed888674325fab84e03b/CHART_VERSION)
* Nginx version which is a [value in the chart](https://gitlab.com/gitlab-org/charts/plantuml/blob/8d080485f58020a08b75a889f1fb81159fa93195/values.yaml#L18)
* PlantUML version which is a [value in the chart](https://gitlab.com/gitlab-org/charts/plantuml/blob/8d080485f58020a08b75a889f1fb81159fa93195/values.yaml#L13).

## Dogfooding

Wherever possible we try to leverage GitLab application features for managing
the deployments on Kubernetes. This includes:

* AutoDevops
* CI pipelines
* Package registry

These issues are tracked in the [Dogfooding Kubernetes features issue board](https://gitlab.com/groups/gitlab-org/-/boards/1284732?label_name[]=Delivery&label_name[]=Dogfooding&label_name[]=group%3A%3Aautodevops%20and%20kubernetes).

## Observability and Monitoring

The Cluster pods are monitored using prometheus and prometheus is installed in the
cluster in its own namespace using the
[prometheus operator helm chart](https://github.com/helm/charts/tree/master/stable/prometheus-operator)

* There are multiple dashboards for monitoring both the GKE cluster and
  performance of the application:
  * [Workloads for PreProd](https://dashboards.gitlab.net/d/kubernetes-resources-workload/kubernetes-compute-resources-workload?orgId=1&refresh=10s&var-datasource=Global&var-cluster=pre-gitlab-gke&var-namespace=plantuml&var-workload=plantuml&var-type=deployment): Monitoring scaling and resources
  * [Pods for PreProd](https://dashboards.gitlab.net/d/kubernetes-resources-pod/kubernetes-compute-resources-pod?orgId=1&refresh=10s&var-datasource=Global&var-cluster=pre-gitlab-gke&var-namespace=plantuml&var-pod=plantuml-7f6b9b6894-nwzfm): Metrics for individual pods
  * [Overview dashboard for prepod](https://dashboards.gitlab.net/d/plantuml-main/plantuml-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=pre&var-cluster=pre-gitlab-gke): Status codes and latencies

* For security auditing, there are Nginx logs that log the source IP of every request to
  the PlantUML service. These logs are forwarded to central logging and are
  structured json.

## Testing

Testing was done in non-production environments for
basic validation and testing the service under load.

### Load testing

* PlantUML is setup with CDN caching for requests after a diagram has been
  created, verified locally for a single diagram up to ~1000 req/second:

```
bombardier -d 120s --connections 20  https://pre.plantuml.gitlab-static.net/png/bPFDRi8m48JlVeeLFNFegIg7IeaZ92HKLK9zW2LUKXEmdRLJe4_V1AJcHw491n_YV3Cxir64pxLno7Y4BpJgn3e4CX2u926QTvwAaanA4RCzINkm30cHs-IN-52H0s0QBCUM5V4KVArtrJ7KLcn7K4ppZ6rfr0r_YbuJcIm751_8GQVQeGdJ4xL5lKc9LHdnk6vfJNZ78y7gw30Du_16_n0yluo3Ourls-_5ngB4UsnZEk6p_X_r3xbMZz3HMGYiB-7J4mlNL87PKhCF80_e_GpGGwjdr2Ktb5gZXj6uYwxndgSKeiu0yxVEf0NiJK6kpFPxjxkSGx5bhR4EhRynK3ULQ3_rdE87dEAEKznpaWyuoRQHXFcqJ8fyO7lfhQSRQTgU58ttYwvczV8NrVzZw8ue4hy0

Statistics        Avg      Stdev        Max
  Reqs/sec       524.56     135.17    1020.26
  Latency       38.10ms     6.75ms   329.17ms
  HTTP codes:
    1xx - 0, 2xx - 62995, 3xx - 0, 4xx - 0, 5xx - 0
    others - 0
  Throughput:    14.18MB/s
```

**For reads we do not expect to experience any issues under load because
all charts are cached with long expiration at the CDN**


* For generating new charts the service can easily fall over without limits so
  these are set in the [helm chart for the nginx sidecar](https://gitlab.com/gitlab-org/charts/plantuml/blob/8d080485f58020a08b75a889f1fb81159fa93195/values.yaml#L24-31).
* https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/plantuml/merge_requests/7
  set rate limits for the pre-prod environment that we will carry forward to
  production. Aggressive limits were tested at 2/second (by IP) for
  generating new charts. This is configurable in the
  [env files](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/plantuml/blob/7850985e67984d363b31ed888674325fab84e03b/pre.yaml#L6-8)

## Logging

All logging for the GKE cluster and all of its service is handled by
StackDriver with a log sink to pubsub. Like with the existing infrastructure,
a pubsubbeat is used to consume logs from pubsub and forward them to
elasticsearch. The container name can be used to filter logs for different
services running in the cluster, for example for production:

JSON logs are configured by default for Nginx which will allow us to monitor the
service during the rollout for rate limiting

Example for pre-production: https://nonprod-log.gitlab.net/goto/1409380492c985230a87b5af5dafe621


## Licensing

PlantUML server https://github.com/plantuml/plantuml-server is licensed under GPLv3 which is not on the list of accepted licenses
https://docs.gitlab.com/ee/development/licensing.html#acceptable-licenses

>  GNU GPL (version 1, version 2, version 3, or any future versions): GPL-licensed libraries cannot be linked to from non-GPL projects.

PlantUML is not being re-packaged or redistributed, only configuration scripts
and a HELM chart for deploying it which will be GitLab code licensed under MIT.

Tracked internally with https://gitlab.com/gitlab-com/gl-infra/delivery/issues/466

## Readiness review participants

1. @cmiskell
1. @estrike 
1.
