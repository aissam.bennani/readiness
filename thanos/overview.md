# Thanos Kubernetes Infra Readiness Review

## Table of Contents

[[_TOC_]]

## Summary

Thanos provides a global service aggregation of Prometheus monitoring data.  Benefits include:

* Single pane of glass query interface.
* Long-term data storage.
* High Availability.
* Global data rule evaluation for metrics and alerting.
* Downsampling for large historical data access.

Summary videos:
* [Intro to Thanos on CNCF](https://www.youtube.com/watch?v=m0JgWlTc60Q)
* [Thanos Deep Dive at CNCF](https://www.youtube.com/watch?v=qQN0N14HXPM)

### Kubernetes migration

In order to reduce toil and improve service we are moving all Thanos components to GKE deployed by Helm.

* Easier horizontal scaling.
* Simplified upgrades and rollbacks.
* Easier testing and pre-deployment simulation.
* Easier sharding for public and internal access.
* More efficient compute resource utilization.

## Documentation

The [upstream documentation](https://thanos.io/tip/thanos/getting-started.md/) covers the vast majority of our documentation needs.

We cover the rest of the GitLab specifics in the [GitLab Runbooks](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/README.md).

For the migration effort we have [a Thanos improvements epic][epic-813]

## Architecture

Thanos in Kubernetes will be deployed and managed with the [official kube-thanos project](https://github.com/thanos-io/kube-thanos).
The deployment configuration is codified in the [k8s-workloads/helm](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/thanos) project.

```mermaid
graph LR
  subgraph ops[Thanos Components in ops]
    thanosQuery[Thanos Query]
    thanosFrontends[Thanos Frontends]
    thanosRule[Thanos Rule]
    alertmanager[Alertmanager]
    thanosFrontendMemcached[Thanos Frontend memcached]
    thanosCompact[Thanos Compact]

    grafana[dashboards.gitlab.net]
    IAP[Google IAP]
  end

  subgraph envs[Cluster Monitoring: gprd, gstg, ops, etc]
    subgraph prometheus[Prometheus Servers]
      thanosSidecar[Thanos Sidecar]
    end

    thanosStore[Thanos Store]
    thanosStoreBucketMemcached[Store Bucket memcached]
    thanosStoreIndexMemcached[Store Index memcached]

    gcsBucket[GCS Bucket]
  end

  thanosQuery --> thanosSidecar
  thanosQuery --> thanosStore
  thanosFrontends --> thanosQuery
  thanosFrontends --> thanosFrontendMemcached

  grafana --> thanosFrontends
  IAP --> thanosFrontends

  thanosRule --> alertmanager
  thanosRule --> thanosQuery

  thanosStore --> gcsBucket
  thanosSidecar --> gcsBucket
  thanosCompact --> gcsBucket
  thanosStore --> thanosStoreBucketMemcached
  thanosStore --> thanosStoreIndexMemcached

style alertmanager fill:#E58A73;
style prometheus fill:#E58A73;
style thanosCompact fill:#BAA6FF;
style thanosFrontends fill:#BAA6FF;
style thanosQuery fill:#BAA6FF;
style thanosRule fill:#BAA6FF;
style thanosSidecar fill:#BAA6FF;
style thanosStore fill:#BAA6FF;
```

#### Deployed assets and locations in repos:

* DNS - thanos.pre.gitlab.net and thanos.gitlab.net in [Terraform](https://gitlab.com/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/ops/monitoring.tf)
* [GCS Buckets](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets)
* [K8s-workloads](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/thanos) 

## Operational Risk Assessment

### Affected Dependencies

The Prometheus+Thanos stack is a critical piece of our observability.

* Thanos Query/Frontend provides access to metrics/dashboards.
* Thanos Rule provides SLO recording and alerting.
* Thanos Compact manages the GCS buckets.
* Thanos Store provides long-term query data.
* Thanos Sidecar backs up Prometheus data to GCS and provides query access.

GCS and GKE are the two primary underlying services in GCP used by the Thanos ecosystem.
Most thanos components are deployed in GKE in the ops GCP project on the ops-gitlab-gke regional cluster.
This provides zonal failure, but a severe failure in the us-east1 region would dimish visibility.

To ensure beyond regional availability, we should:
1. Evaluate if the prometheus GCS buckets are multi-regional
2. Make sure the rules bucket is multi-regional
3. Investigate / create runbooks for spinning up thanos deployment via K8s-workloads in alternate regions.

#### Thanos Query/Frontend

The Thanos Query and Query Frontend provide a high-availiabilty Prometheus-compatible
query UI and API for operators to access all metric data as a "single pane of glass".

Thanos Query also provides automatic access to downsampled data and deduplication of
high-availiabilty Prometheus scrape data.

The primary access is via Grafana, but there is also a direct IAP to the Thanos Query
UI, which is similar to Prometheus, but provides controls for downsampling and
deduplication.

The Query/Frontend are susceptible to user-driven load problems. There are a number of
configuration controls in place to limit the per-query data size, and the number of
concurrent queries. This is done to avoid overloading downstream storage performance
issues. In addition, large query load can affect the operation of Prometheus instances
as Thanos uses them for access to the most recent data (latest 24 hours).

#### Thanos Rule

The Thanos Rule component records aggregated metrics from all of our Prometheus
servers in order to produce SLO-based alerts.

The Thanos rule component, like Prometheus, runs multiple high-availiabity
instances to avoid single instance failure.

Thanos Rule can be affected by label cardinality issues generated by scraped
Prometheus data. The rules generated/stored in the [runbooks][runbooks] need
to be managed to avoid overloading of the Rule service.

There are meta-monitoring alerts in place to detect rules that are overloading
the service.

Thanos Rule is also meta-monitored via heartbeat pings to DeadMan Snitch service.

Runbooks: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/thanos-rule.md

#### Thanos Compact

Thanos Comapact is a single concurrency per environment service to manage the
data stored in each GCS bucket. It continuously scans the bucket for work to do.

* Compact smaller blocks into larger blocks (i.e. 2 hours -> 6 hours)
* Generate downsample blocks for faster long-term queries (5 minute and 1 hour blocks)
* Delete old data based on the retention policie(s).

Thanos Compact is a background service, it should be generally available, but there is
minimal risk of issues if it is down.  Splitting the compactor pods by environment protects one 
environment compaction failure from affecting other compactors.

The current retention policy is one year for raw data and unlimited retention of
downsample data.

Runbooks: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/thanos-compact.md

#### Thanos Store

Thanos Store provides a horizontally-sharded, high-availability caching data
service in front of the GCS object storage. This provides the Thanos Query access
to all of the stored historical data. It is operated per environment so that it
can be scaled depending on the amount of data generated in that environment.

Currently Thanos Store provides access to data older than 24 hours.

Runbooks: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/thanos-store.md

#### Thanos Sidecar

The Thanos Sidecars are responsible for uploading Prometheus TSDB blocks to GCS. It also provides
access to the current 24 hours of metric data to the Thanos Query service.

The TSDB blocks are generated every 2 hours by Prometheus.

## Rollout

All of the steps to rollout this change are detailed in [gl-infra Epic 283][epic-283].

## Rollback

We will operate simultaneous deployment in Chef and Kubernetes to allow for instant rollback by updating the "Global" datasource in the Grafana front-end.

## Data and Retention

Thanos stores data in GCS buckets.  The current data classification is orange per our [Classifcation Index](https://about.gitlab.com/handbook/security/data-classification-standard.html) linked in the Handbook.  The data stored is metrics for systems, but not personal information of GitLab.com users.  From the past, we have classified this as internal and orange since we can infer usage of GitLab.com from the data.

* The bucket configuration is [managed by Terraform](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets/-/blob/master/bucket_prometheus.tf).  
* Thanos Compact component manages the retention policy of the data.
  * 1 Year of raw metrics.
  * Unlimited retention of downsampled metrics.
* GCS also maintain 14 days of object versions for restoration of unintended changes.

## Testing and Performance

There are 2 thanos deployments:
1. Main - [thanos.gitlab.net](https://thanos.gitlab.net) - connected to most production/infra deployments (gprd, gstg, prdsub, etc)
2. Pre (For testing) - [thanos.pre.gitlab.net](https://thanos.pre.gitlab.net) - connected to only the [pre environment](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#pre).  

The [main folder for Thanos on dashboards.gitlab.net](https://dashboards.gitlab.net/dashboards/f/thanos/thanos) will be updated and linked in relevant runbooks
Thanos has tracing documented in [this runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/thanos-tracing.md)

For monitoring performance, Thanos is now its own service in the [service catalog](https://gitlab.com/gitlab-com/runbooks/-/blob/master/services/service-catalog.yml) with SLO monitoring.  Alerts are mainly going to the #g_infra_observability_alerts channel and will be adjusted as the team matures.  Spikes in usage will mainly be via incident/heavy usage of Grafana or new large cardinality evaluations by the Thanos Ruler.  


## Security

We will continue to use the same GCP IAP access controls. Only internal gitlab.com users are authorized.

## Backup and Restore

We will continue to use the same GCS object storage buckets that provide automatic versioning and deletion management.

## Monitoring and Alerts

We will continue to use the same SLO metics and alerts.
Thanos now has [its own service in the metrics catalog](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/14644) and [relevant SLOs are recorded there](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/thanos.jsonnet).

## Responsibility

* Reliability - Team::Observability
* subject matter experts:
  * @nduff
  * @knottos

## Readiness Review Participants

* @knottos
* @nduff
* @dawsmith

[epic-813]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/813
[runbooks]: https://gitlab.com/gitlab-com/runbooks
[issue-16758]: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16758 
